MapView = Backbone.View.extend({
	
	RESOLUTION_BOUNDS: {
        20: [[964708, 964710], [629236, 629237]],
        21: [[1929416, 1929421], [1258472, 1258474]],
        22: [[3858832, 3858842], [2516944, 2516948]]
    },

	MIN_RESOLUTION: 20,

	MAX_RESOLUTION: 22,
	
	TILE_TEMPLATE_URL: 'tiles/L{L}_{Z}_{X}_{Y}.png',

	events: {
		"click .map-side-panel-trigger": "toggleSidePanel",
		"click .map-popover": "markerClick"
	},

	initialize: function() {
		this.markers = {};

		personLocations.each(function(entity) {
			var model = employees.get(entity.get('id'));
			if (model && !model.get('termination_date')) {
				this.markers[entity.get('level')] = this.markers[entity.get('level')] || {};
				var marker = new RichMarker({
					position: new google.maps.LatLng(entity.get('lat'), entity.get('lng')),
					content: new PersonPopoverView({
						model: employees.get(entity.get('id'))
					}).render().el,
					anchor: new google.maps.Size(-34/2, -42)
				});
				this.markers[entity.get('level')][entity.get('id')] = marker;
			} else {
				console.warn(entity.get('id') + " does not exist");
			}
		}, this);

		miscLocations.each(function(entity) {
			this.markers[entity.get('level')] = this.markers[entity.get('level')] || {};
			var marker = new RichMarker({
				position: new google.maps.LatLng(entity.get('lat'), entity.get('lng')),
				content: new MiscPopoverView({
					model: entity
				}).render().el,
				anchor: new google.maps.Size(-36/2, -40)
			});
			this.markers[entity.get('level')][entity.get('id')] = marker;
		}, this);
	},

	getTileUrl: function(coord, zoom) {
	  if (this.MIN_RESOLUTION > zoom || zoom > this.MAX_RESOLUTION) {
	      return '';
	  }

	  if ((this.RESOLUTION_BOUNDS[zoom][0][0] > coord.x || coord.x > this.RESOLUTION_BOUNDS[zoom][0][1]) ||
	      (this.RESOLUTION_BOUNDS[zoom][1][0] > coord.y || coord.y > this.RESOLUTION_BOUNDS[zoom][1][1])) {
	    return '';
	  }

	  return this.TILE_TEMPLATE_URL.replace('{L}', this.currentLevel).replace('{Z}', zoom).replace('{X}', coord.x).replace('{Y}', coord.y);
	},

	updateOverlay: function() {
	    var that = this;
	    var overlay = new google.maps.ImageMapType({
	        getTileUrl: function(coord, zoom) {
	            return that.getTileUrl(coord, zoom);
	        },
	        tileSize: new google.maps.Size(256, 256)
	    });

		this.map.overlayMapTypes.clear();
	    this.map.overlayMapTypes.push(overlay);
    },
	
	showLevel: function(level) {
	    if (level != this.currentLevel) {
	    	var prevLevel = this.currentLevel;
	    	this.currentLevel = level;
	    	this.$(".active").removeClass("active");
			this.$("#level-"+level).addClass("active");
			this.updateOverlay();

			if (prevLevel) {
				_.each(this.markers[prevLevel], function(marker) {
					marker.setMap(null);
				});
			}

			_.each(this.markers[level], function(marker) {
				marker.setMap(this.map);
			}, this);
		}
	},

    render: function() {
        this.$el.html(this.template({
        	isPanelClosed: this.isPanelClosed() ? "closed" : ""
        }));

        this.$("#inputUsername").typeahead({
            source: _.pluck(_.filter(employees.toJSON(), function(employee) {
            	return !employee.termination_date;
            }), 'username')
        });

		this.map = new google.maps.Map(this.$('#map-canvas').get(0), {
		    zoom: 20,
			minZoom: 20,
		    center: new google.maps.LatLng(-33.8672, 151.2067),
		    mapTypeControl: false,
		    panControl: false,
		    streetViewControl: false,
		    mapTypeId: google.maps.MapTypeId.HYBRID,
			styles: [{
    			"featureType": "poi.business",
				"stylers": [{ "visibility": "off" }]
  			}]
		});

		var that = this;
        google.maps.event.addListener(this.map, 'click', function(e) {
        	that.$('.focus').removeClass("focus");
            that.$("#inputLatitude").val(e.latLng.lat());
            that.$("#inputLongitude").val(e.latLng.lng());
            that.$("#inputLevel").val(that.currentLevel);
        });

        return this;
    },

    markerClick: function(e) {
    	this.showMarker($(e.currentTarget).data('id'));
    },
	
	showMarker: function(id) {
		var entity = personLocations.get(id) || miscLocations.get(id);
		if (entity) {
	        var latLng = new google.maps.LatLng(entity.get('lat'), entity.get('lng'));
	        this.map.panTo(latLng);
	        this.map.setZoom(this.MAX_RESOLUTION);
	        this.showLevel(entity.get('level'));
	        this.$('.focus').removeClass("focus");
	        $(this.markers[entity.get('level')][id].getContent()).addClass("focus");
		}
	},

	toggleSidePanel: function(e) {
		e.preventDefault();
		this.$(".map-side-panel").toggleClass("closed", this.togglePanelClosed());
	},

	isPanelClosed: function() {
		return !!JSON.parse(localStorage.getItem("closed"));
	},

	setPanelClosed: function(closed) {
		localStorage.setItem("closed", closed);
	},

	togglePanelClosed: function() {
		var closed = !this.isPanelClosed();
		this.setPanelClosed(closed);
		return closed;
	}

});

PersonPopoverView = Backbone.View.extend({

    render: function() {
        this.$el.html(this.template(_.extend(this.model.toJSON(), {
        	className: departmentToCssClass(this.model.get('department'))
        })));
        return this;
    }

});

MiscPopoverView = Backbone.View.extend({

    render: function() {
        this.$el.html(this.template(_.extend(this.model.toJSON())));
        return this;
    }

});
