SearchView = Backbone.View.extend({
	
	tagName: "input",
	
	className: "input-xlarge",
	
	attributes: {
		type: "search",
		placeholder: "Search Atlassian Directory",
		autocomplete: "off",
		autofocus: true
	},

    render: function() {
        this.$el.typeahead({
            source: _.filter(employees.toJSON(), function(employee) {
            	return !employee.termination_date;
            }).concat(miscLocations.toJSON()),
			matcher: function(item) {
				var regex = new RegExp(this.query, "i");
				if (item.type == "Person") {
					return regex.test(item.full_name) && item.username;
				} else {
					return regex.test(item.name);
				}
			},
			sorter: function(items) {
				return _.sortBy(items, function(item) {
					return [item.type, item.full_name, item.name];
				});
    		},
			highlighter: function(item) {
		        var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
		        var regex = new RegExp('(' + query + ')', 'ig');
		        if (item.type == "Person") {
			        return item.full_name.replace(regex, function ($1, match) {
			            return '<strong>' + match + '</strong>';
			        });
			    } else {
			    	return item.name.replace(regex, function ($1, match) {
			            return '<strong>' + match + '</strong>';
			        });
			    }
		    },
		    items: 25,
		    menu: '<ul class="typeahead dropdown-menu navbar-search-dropdown"></ul>'
        });
		
		var that = this.$el.data('typeahead');

		that.select = function() {
      	    var href = this.$menu.find('.active a').attr('href');
      	    if (href) {
      	    	window.location.assign(href);
				this.$element.val('');
            	this.hide();
            }
            return this;
    	};

		that.render = function(items) {
			var currentType = "";
			var $menu = this.$menu.empty();
			_.each(items, function(item) {
				if (item.type != currentType) {
					$menu.append(new CategorySearchItemView({
						model: item
					}).render().el);
					currentType = item.type;
				}
				var view;
				if (item.type == "Person") {
					view = new EmployeeListItemView({
						model: employees.get(item.username)
					}).render().$el.data('value', item);
				} else {
					view = new MiscSearchItemView({
						model: item
					}).render().$el;
				}
                view.find('.name').html(that.highlighter(item));
                $menu.append(view);
			});
			$menu.children("li").has("a").first().addClass("active");
            return this;
        };

        that.next = function() {
			var active = this.$menu.find('.active').removeClass('active'), next = active.nextAll().has("a").first();

			if (!next.length) {
				next = this.$menu.find('li').has("a").first();
				this.$menu.scrollTop(0);
			}

			next.addClass('active');

			if (next.position().top + next.height() > this.$menu.height()) {
				next.get(0).scrollIntoView(false);
			}
		};

		that.prev = function() {
			var active = this.$menu.find('.active').removeClass('active'), prev = active.prevAll().has("a").first();

			if (!prev.length) {
				prev = this.$menu.find('li').has("a").last();
				this.$menu.scrollTop(this.$menu.prop('scrollHeight') - this.$menu.height());
			}

			prev.addClass('active');

			if (prev.position().top < 0) {
				prev.get(0).scrollIntoView(true);
			}
		};

        return this;
	}

});

SearchItemView = Backbone.View.extend({

    tagName: "li",

    render: function() {
        this.$el.html(this.template(this.model));
        return this;
    }

});

MiscSearchItemView = SearchItemView.extend();

CategorySearchItemView = SearchItemView.extend();