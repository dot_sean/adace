BreadcrumbView = Backbone.View.extend({

	tagName: "ul",
	
	className: "breadcrumb",
	
	initialize: function() {
		this.model = new Backbone.Collection();
		for (var model = this.options.model; model; model = model.get('manager')) {
			this.model.unshift(model);
		}
	},

    render: function() {
        this.model.each(function(employee, index, list) {
            this.$el.append(new BreadcrumbItemView({
				model: employee,
				last: index === (list.length - 1)
			}).render().el);
        }, this);
        return this;
    }

});

BreadcrumbItemView = Backbone.View.extend({
	tagName: "li",

	render: function() {
		this.$el.toggleClass('active', this.options.last);
		
		this.$el.html(this.template(_.extend(this.model.toJSON(), {
			last: this.options.last
		})));
		return this;
	}
});