HomeView = Backbone.View.extend({

    render: function() {
        this.$el.html(this.template());
        this.$('.navbar-search').html(new SearchView().render().el);
        return this;
    }

});