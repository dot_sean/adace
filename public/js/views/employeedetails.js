EmployeeView = Backbone.View.extend({

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$('#details').append(new EmployeeSummaryView({
			model: this.model
		}).render().el);

		var reports = this.model.get('reports');

        if (reports.length) {
	        this.$('#reports').append(new EmployeeListView({
				model: reports
			}).render().el);
        } else {
            this.$('.no-reports').show();
        }
        return this;
    }
});

EmployeeSummaryView = Backbone.View.extend({

    initialize: function() {
        this.model.bind("change", this.render, this);
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }

});