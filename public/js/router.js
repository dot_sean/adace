Router = Backbone.Router.extend({

    routes: {
        "": "home",
		"map/:id(/)": "map",
        "map(/)": "map",
        "employees/:id(/)": "employeeDetails",
        "employees(/)": "employeeDetails"

    },

    initialize: function() {
    },


    home: function() {
        if (!this.homeView) {
            this.homeView = new HomeView();
            this.homeView.render();
        }
        $("#content").html(this.homeView.el);
        $('#breadcrumb').empty();
        $('.navbar-search').html(new SearchView().render().el);
        currentMode = "home";
        AP.resize();
        self.scrollTo(0,0)
    },

    map: function(id) {
        if (!this.mapView) {
            this.mapView = new MapView();
            this.mapView.render();
        }
        $('#content').html(this.mapView.el);
        this.mapView.delegateEvents();
		currentMode = "map";
		var level = parseInt(id, 10);
		if (!isNaN(level)) {
			this.mapView.showLevel(level);
		} else if (id) {
			var employee = employees.get(id);
			$('#breadcrumb').html(new BreadcrumbView({
				model: employee
			}).render().el);
            $('#breadcrumb').prepend('<form class="navbar-search pull-right"></form>');
			this.mapView.showMarker(id);
		} else {
			this.mapView.showLevel(2);
		}

        $('.navbar-search').html(new SearchView().render().el);
        AP.resize();
        self.scrollTo(0,0);
    },

    employeeDetails: function(id) {
		id = id || '';
		var employee = employees.get(id);
        $('#content').html(new EmployeeView({
			model: employee
		}).render().el);
		$('#breadcrumb').html(new BreadcrumbView({
			model: employee
		}).render().el);
        $('#breadcrumb').prepend('<form class="navbar-search pull-right"></form>');

        $('.navbar-search').html(new SearchView().render().el);
		currentMode = "employees";
        AP.resize();
        self.scrollTo(0,0);
    }

});