// The Template Loader. Used to asynchronously load templates located in separate .html files
templateLoader = {
    load: function(views) {
        var deferreds = [];
        _.each(views, function(view) {
            deferreds.push($.get('tpl/' + view + '.html', function(data) {
                window[view].prototype.template = _.template(data);
            }));
        });
        return $.when(deferreds);
    }
};

function departmentToCssClass(name) {
	return name.toLowerCase().replace(/\W+/g, "-");
}

