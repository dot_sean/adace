(function() {
	$.when(new EmployeeCollection().fetch({
		success: function(collection) {
			//add a dummy employee to be the root
			collection.add({
				full_name: "<img src='img/atlassian-charlie.svg' style='width: 3em;'/>"
			});

			var i = 0;
			collection.each(function(employee) {
				//prevent the dummy employee from recursively referencing itself
				if (employee.get('username')) {
					if (!employee.get('termination_date')) {
						var manager = collection.get(employee.get('manager_username'));
						if (manager) {
							employee.set('manager', manager);
							manager.get('reports').add(employee);
						}
					}
					employee.set('number', ++i);
				}
			});

			//call getDescendants on the root employee so it'll prepopulate the descendants property of every employee
			collection.get('').getDescendants();

			employees = collection;
		}
	}), new PersonLocationCollection().fetch({
		success: function(collection) {
			personLocations = collection;
		}
	}), new MiscLocationCollection().fetch({
		success: function(collection) {
			miscLocations = collection;
		}
	}), templateLoader.load([
		"HomeView",
		"MapView",
		"HeaderView",
		"EmployeeView",
		"EmployeeSummaryView",
		"EmployeeListItemView",
		"BreadcrumbItemView",
		"PersonPopoverView",
		"MiscPopoverView",
		"MiscSearchItemView",
		"CategorySearchItemView"
	])).done(function() {
		app = new Router();
		Backbone.history.start();
	}).fail(function() {
		$("#progress-bar").addClass("hidden");
		$("#error-alert").removeClass("hidden").addClass("in");
		console.error(arguments);
	});
})();
