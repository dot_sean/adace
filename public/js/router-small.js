Router = Backbone.Router.extend({

    routes: {
        "": "home",
    },

    initialize: function() {
    },


    home: function(id) {

        // extract querystring
        var vars = [], hash;
        var q = document.URL.split('?')[1];
        if(q != undefined){
            q = q.split('&');
            for(var i = 0; i < q.length; i++){
                hash = q[i].split('=');
                vars.push(hash[1]);
                vars[hash[0]] = hash[1];
            }
        }




        id = id || vars['user.id'];
        var employee = employees.get(id);
        $('#content').html(new EmployeeSummaryView({
            model: employee
        }).render().el);
        currentMode = "employeeSummaryView";
        AP.resize();
        self.scrollTo(0,0)
    }


});