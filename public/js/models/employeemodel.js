Employee = BaseModel.extend({

    idAttribute: "username",

    defaults: function() {
    	return {
        	username: "",
        	status: "",
        	first_name: "",
        	last_name: "",
			full_name: "",
  		    name_last_first: "",
 		    email: "",
  		    division: "",
        	department: "",
   		    location: "",
        	title: "",
        	hire_date: "",
        	phone_ext: "",
        	phone_mobile: "",
        	manager_username: "",
        	office_location: "",
        	employment_status: "",
        	birthday: "",
        	termination_date: "",
        	gender: "",
        	last_changed: "",
        	bamboohr_id: "",
        	operating_system: "",
        	machine_preference: "",
        	sysadmin_updates: "",
        	middle_name: "",
        	nickname: "",
        	fte: "",
        	market_job_code: "",
        	market_job_function: "",
        	salary_review_manager: "",
			manager: null,
			reports: new EmployeeSearchCollection(),
			descendants: null,
			type: "Person",
			number: 0
    	}
    },
	
	toJSON: function(options) {
		var doNotSerialize = options && options.doNotSerialize;
		var json = _.clone(this.attributes);
		if (!doNotSerialize) {
			json.manager = json.manager && json.manager.toJSON({
				doNotSerialize: true
			});
		}
		return json;
	},

	getDescendants: function() {
		if (this.get('descendants') === null) {
			this.set('descendants', this.get('reports').reduce(function(memo, employee) {
				return memo + employee.getDescendants();
			}, this.get('reports').length));
		}
		return this.get('descendants');
	}

});

EmployeeCollection = Backbone.Collection.extend({

    model: Employee,

    url: "employees",
	
	comparator: function(employee) {
		return employee.get('hire_date');
	}

});

EmployeeSearchCollection = EmployeeCollection.extend({

	comparator: function(employee) {
		return employee.get('full_name');
	},

	add: function(models, options) {
		models = _.isArray(models) ? models.slice() : [models];
		models = _.filter(models, function(employee) {
			employee = this._prepareModel(employee);
			return !employee.get('termination_date') && Date.parse(employee.get('hire_date')) <= Date.now();
		}, this);
		return Backbone.Collection.prototype.add.call(this, models, options);
	}

});