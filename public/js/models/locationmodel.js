Location = BaseModel.extend({

    defaults: {
        desc: "",
        lat: 0.0,
        lng: 0.0,
        level: 0
    }

});

PersonLocation = Location.extend({

    defaults: {
        type: "Person"
    }

});

PersonLocationCollection = Backbone.Collection.extend({

    model: PersonLocation,

    url: "https://spreadsheets.google.com/feeds/list/0AhTnzncg0f1JdEF6VDJoaXJxWDJCWWh6dllVNVJQNFE/od6/public/values?alt=json",

    parse: function(json) {
        var map = {};
        _.each(json.feed.entry, function(entry) {
            entry = {
                id: entry.gsx$id.$t,
                lat: parseFloat(entry.gsx$latitude.$t),
                lng: parseFloat(entry.gsx$longitude.$t),
                level: parseInt(entry.gsx$level.$t, 10)
            };
            map[entry.id] = entry;
        });
        return _.values(map);
    }

});

MiscLocationCollection = Backbone.Collection.extend({

    model: Location,

    url: "https://spreadsheets.google.com/feeds/list/0AhTnzncg0f1JdEF6VDJoaXJxWDJCWWh6dllVNVJQNFE/od7/public/values?alt=json",

    parse: function(json) {
        return _.map(json.feed.entry, function(entry) {
            return {
                id: entry.gsx$name.$t.replace(/\s/g, '-').replace(/[^\w\-]/g, '').toLowerCase(),
                name: entry.gsx$name.$t,
                desc: entry.gsx$description.$t,
                type: entry.gsx$type.$t,
                lat: parseFloat(entry.gsx$latitude.$t),
                lng: parseFloat(entry.gsx$longitude.$t),
                level: parseInt(entry.gsx$level.$t, 10)
            };
        });
    }

});

