# anchor a latitude/longitude to a given point on each image
ANCHOR_LAT_LON=-33.867065,151.20627
ANCHOR_IMG_PT=0.028998,0.0

mkdir -p out/

./gmaps-tiler.py "levels-png/L2_20.png" 20 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L2_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L2_21.png" 21 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L2_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L2_22.png" 22 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L2_%z_%x_%y.png

./gmaps-tiler.py "levels-png/L3_20.png" 20 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L3_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L3_21.png" 21 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L3_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L3_22.png" 22 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L3_%z_%x_%y.png

./gmaps-tiler.py "levels-png/L6_20.png" 20 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L6_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L6_21.png" 21 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L6_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L6_22.png" 22 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L6_%z_%x_%y.png

./gmaps-tiler.py "levels-png/L7_20.png" 20 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L7_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L7_21.png" 21 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L7_%z_%x_%y.png
./gmaps-tiler.py "levels-png/L7_22.png" 22 $ANCHOR_LAT_LON $ANCHOR_IMG_PT out/L7_%z_%x_%y.png
